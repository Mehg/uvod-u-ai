package ui;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Clause {
    private final HashSet<Literal> literals;
    private Clause firstParent;
    private Clause secondParent;
    private int number;

    public Clause() {
        this.literals = new HashSet<>();
    }

    public Clause(Literal literal) {
        this();
        literals.add(literal);
    }

    public Clause(HashSet<Literal> literals) {
        this.literals = literals;
    }

    public void addLiteral(Literal literal) {
        literals.add(literal);
    }

    public boolean containsLiteral(Literal literal) {
        return literals.contains(literal);
    }

    public boolean containsNegated(Literal literal) {
        Literal negated = new Literal(literal);
        negated.negate();
        return literals.contains(negated);
    }

    public Set<Clause> negate() {
        HashSet<Clause> retSet = new HashSet<>();

        for (Literal literal : literals) {
            Literal literal1 = new Literal(literal);
            literal1.negate();
            retSet.add(new Clause(literal1));
        }

        return retSet;
    }

    public HashSet<Literal> getLiterals() {
        return literals;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Clause getFirstParent() {
        return firstParent;
    }

    public void setFirstParent(Clause firstParent) {
        this.firstParent = firstParent;
    }

    public Clause getSecondParent() {
        return secondParent;
    }

    public void setSecondParent(Clause secondParent) {
        this.secondParent = secondParent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clause clause = (Clause) o;
        return Objects.equals(literals, clause.literals);
    }

    @Override
    public int hashCode() {
        return Objects.hash(literals);
    }

    @Override
    public String toString() {
        return literals.stream()
                .map(Literal::toString)
                .collect(Collectors.joining(" v "));
    }
}
