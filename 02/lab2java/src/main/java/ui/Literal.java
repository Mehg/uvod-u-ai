package ui;

import java.util.Objects;

public class Literal {
    private String name;
    private boolean negated;

    public Literal() {
        name = "";
        negated = false;
    }

    public Literal(String name) {
        this();
        this.name = name;
    }

    public Literal(Literal literal) {
        this.name = literal.name;
        this.negated = literal.negated;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNegated(boolean negated) {
        this.negated = negated;
    }

    public String getName() {
        return name;
    }

    public boolean isNegated() {
        return negated;
    }

    public void negate() {
        setNegated(!isNegated());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Literal literal = (Literal) o;
        return negated == literal.negated && Objects.equals(name, literal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, negated);
    }

    @Override
    public String toString() {
        if (negated)
            return "~" + name;
        else return name;
    }
}
