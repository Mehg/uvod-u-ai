package ui;

import java.util.*;

public class Resolution {
    private final ArrayList<Clause> clauses;
    private final ArrayList<Clause> setOfSupport;
    private final Clause goal;
    private boolean nilGenerated;
    private final StringBuilder sb;
    private int nextClauseNumber;

    private final ArrayList<Clause> startingClauses;
    private Set<Clause> usedClauses = new TreeSet<>(Comparator.comparing(Clause::getNumber));
    private final Clause NIL = new Clause(new Literal("NIL"));

    public Resolution(HashSet<Clause> clauses, Clause goal) {
        this.clauses = new ArrayList<>(clauses);
        this.goal = goal;
        this.setOfSupport = new ArrayList<>(goal.negate());
        this.sb = new StringBuilder();
        this.nilGenerated = false;
        this.nextClauseNumber = 1;
        this.startingClauses = new ArrayList<>(clauses);
        startingClauses.addAll(setOfSupport);
    }

    public void run() {
        generateHeader();

        removeRedundant(clauses);
        removeUseless(clauses);
        removeRedundant(setOfSupport);
        removeUseless(setOfSupport);


        while (true) {

            ArrayList<Clause> union = new ArrayList<>(clauses);
            union.addAll(setOfSupport);
            HashSet<Clause> copyOfSoS = new HashSet<>(setOfSupport);

            for (Clause first : union) {
                for (Clause second : copyOfSoS) {
                    if (first.equals(second))
                        continue;

                    if (canResolve(first, second)) {
                        Set<Clause> resolvent = resolve(first, second);


                        resolvent.forEach(r -> {
                                    r.setFirstParent(first);
                                    r.setSecondParent(second);
                                    r.setNumber(nextClauseNumber++);
                                }
                        );

                        if (resolvent.contains(NIL)) {
                            nilGenerated = true;
                            return;
                        }

                        resolvent.removeIf(union::contains);

                        setOfSupport.addAll(resolvent);
                    }
                }
            }
            removeRedundant(setOfSupport);
            removeUseless(setOfSupport);

            if (union.containsAll(setOfSupport)) {
                nilGenerated = false;
                return;
            }
        }
    }

    private boolean canResolve(Clause first, Clause second) {
        for (Literal literal : first.getLiterals())
            if (second.containsNegated(literal))
                return true;
        return false;
    }

    private Set<Clause> resolve(Clause first, Clause second) {
        HashSet<Clause> retSet = new HashSet<>();

        for (Literal literal : first.getLiterals()) {
            if (second.containsNegated(literal)) {
                HashSet<Literal> resolventLiterals = new HashSet<>(first.getLiterals());
                resolventLiterals.addAll(second.getLiterals());
                resolventLiterals.remove(literal);
                literal.negate();
                resolventLiterals.remove(literal);
                literal.negate();

                if (resolventLiterals.isEmpty())
                    retSet.add(NIL);
                else
                    retSet.add(new Clause(resolventLiterals));
            }
        }
        return retSet;
    }

    private void removeRedundant(Collection<Clause> givenClauses) {
        Collection<Clause> copy = new HashSet<>(givenClauses);
        HashSet<Clause> union = new HashSet<>(clauses);
        union.addAll(setOfSupport);
        union.addAll(copy);

        l:
        for (Clause clause : copy) {

            notRemoved:
            for (Clause other : union) {

                if (other.equals(clause))
                    continue;

                for (Literal literal : clause.getLiterals()) {
                    if (!other.containsLiteral(literal)) {
                        continue notRemoved;
                    }
                }

                Clause toRemove = clause.getLiterals().size() > other.getLiterals().size() ? clause : other;

                if (clauses.contains(toRemove))
                    clauses.remove(toRemove);
                else if (setOfSupport.contains(toRemove))
                    setOfSupport.remove(toRemove);
                else
                    givenClauses.remove(toRemove);
                continue l;
            }
        }
    }

    private void removeUseless(Collection<Clause> givenClauses) {
        for (Clause clause : givenClauses) {
            for (Literal literal : clause.getLiterals()) {
                if (clause.containsNegated(literal)) {
                    givenClauses.remove(clause);
                }
            }
        }
    }

    private void generateHeader() {
        for (Clause clause : clauses) {
            clause.setNumber(nextClauseNumber);
            sb.append(nextClauseNumber++).append(". ")
                    .append(clause)
                    .append("\n");
        }
        for (Clause clause : setOfSupport) {
            clause.setNumber(nextClauseNumber);
            sb.append(nextClauseNumber++).append(". ")
                    .append(clause)
                    .append("\n");
        }
        sb.append("===============\n");
    }

    public String getOutput() {
        if (!nilGenerated) {
            return "[CONCLUSION]: " + goal + " is unknown";
        }
        usedClauses.add(NIL);
        getUsed(NIL);
        normalizeNumbers();
        appendPath();
        sb.append("===============\n");
        sb.append("[CONCLUSION]: ")
                .append(goal)
                .append(" is true");

        return sb.toString();
    }

    private void getUsed(Clause clause) {
        Clause firstParent = clause.getFirstParent();
        if (firstParent != null) {
            usedClauses.add(firstParent);
            getUsed(firstParent);
        }
        Clause secondParent = clause.getSecondParent();
        if (secondParent != null) {
            usedClauses.add(secondParent);
            getUsed(secondParent);
        }
    }

    private void normalizeNumbers() {
        ArrayList<Clause> temp = new ArrayList<>(usedClauses);
        temp.removeAll(startingClauses);
        int nextIndex = startingClauses.size() + 1;

        for (Clause clause : temp) {
            int clauseNumber = clause.getNumber();
            if (clauseNumber != nextIndex) {
                clause.setNumber(nextIndex);
            }
            nextIndex++;
        }
        usedClauses = new TreeSet<>(Comparator.comparing(Clause::getNumber));
        usedClauses.addAll(temp);
    }

    private void appendPath() {
        for (Clause clause : usedClauses) {
            sb.append(clause.getNumber())
                    .append(". ")
                    .append(clause)
                    .append(" (")
                    .append(clause.getFirstParent().getNumber())
                    .append(", ")
                    .append(clause.getSecondParent().getNumber())
                    .append(")\n");
        }
    }
}
