package ui;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class Solution {
    private static String flag;
    private static String clausesPath;
    private static String userPath;

    private static final HashSet<Clause> clauses = new HashSet<>();
    private static final ArrayList<String> userCommands = new ArrayList<>();
    private static Clause goal;


    public static void main(String... args) {
        flag = args[0].toLowerCase().trim();
        clausesPath = args[1].toLowerCase().trim();
        if (flag.equals("cooking")) {
            userPath = args[2].toLowerCase().trim();
            loadUserCommands();
        }

        loadClauses();

        if (flag.equals("cooking"))
            handleCooking();
        else handleResolution();
    }

    private static void loadClauses() {
        List<String> lines = null;
        try {
            lines = Files.readAllLines(Path.of(clausesPath));
        } catch (IOException e) {
            System.err.println("IOException " + e.getMessage());
            System.exit(-1);
        }

        lines = lines.stream()
                .filter(l -> !l.startsWith("#"))
                .map(String::toLowerCase)
                .collect(Collectors.toList());

        if (flag.equals("resolution")) {
            String last = lines.remove(lines.size() - 1);
            goal = extractClause(last);
        }

        for (String line : lines) {
            clauses.add(extractClause(line));
        }
    }

    private static Clause extractClause(String last) {
        Clause clause = new Clause();
        String[] parts = last.split(" v ");
        for (String part : parts) {
            Literal literal = new Literal();
            if (part.startsWith("~")) {
                literal.negate();
                part = part.substring(1);
            }
            literal.setName(part);
            clause.addLiteral(literal);
        }
        return clause;
    }

    private static void loadUserCommands() {
        List<String> lines = null;

        try {
            lines = Files.readAllLines(Path.of(userPath));
        } catch (IOException e) {
            System.err.println("IOException " + e.getMessage());
            System.exit(-1);
        }

        userCommands.addAll(lines);
    }

    private static void handleCooking() {
        for (String command : userCommands) {
            System.out.println("User's command: "+command);
            String operation = command.substring(command.length() - 1);
            String clause = command.substring(0, command.length() - 1).trim().toLowerCase();

            switch (operation) {
                case "+" -> addClause(clause);
                case "-" -> removeClause(clause);
                case "?" -> checkClause(clause);
            }

        }
    }

    private static void addClause(String givenClause) {
        Clause clause = extractClause(givenClause);
        clauses.add(clause);
        System.out.println("Added " + clause);
        System.out.println();
    }

    private static void removeClause(String givenClause) {
        Clause clause = extractClause(givenClause);
        clauses.remove(clause);
        System.out.println("Removed " + clause);
        System.out.println();
    }

    private static void checkClause(String givenClause) {
        goal = extractClause(givenClause);
        Resolution resolution = new Resolution(clauses, goal);
        resolution.run();
        System.out.println(resolution.getOutput());
        System.out.println();
    }

    private static void handleResolution() {
        Resolution resolution = new Resolution(clauses, goal);
        resolution.run();
        System.out.println(resolution.getOutput());
    }
}
