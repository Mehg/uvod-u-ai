package ui;

import ui.algorithms.ASTAR;
import ui.algorithms.Algorithm;
import ui.algorithms.BFS;
import ui.algorithms.UCS;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Solution {
    private static String stateSpacePath;
    private static String heuristicPath;
    private static boolean checkOptimistic;
    private static boolean checkConsistent;
    private static String algorithm;
    private static boolean heuristicLoaded;

    private static String output;

    public static Node initial;
    public static final Set<Node> goal = new HashSet<>();
    public static final Map<String, Node> states = new TreeMap<>();
    public static final Map<String, Double> heuristicMap = new HashMap<>();
    public static final Map<String, Map<String, Double>> transitions = new ConcurrentHashMap<>();

    private static final int threadNo = Runtime.getRuntime().availableProcessors();
    private static final ExecutorService threadPool = Executors.newFixedThreadPool(threadNo);

    public static void main(String... args) {
        handleArguments(args);
        loadStateSpace();

        Algorithm alg = null;
        switch (algorithm) {
            case "bfs" -> alg = new BFS(initial);
            case "ucs" -> alg = new UCS(initial);

            case "astar" -> {
                loadHeuristic();
                alg = new ASTAR(initial, heuristicPath);
            }
            default -> {
                if (!checkConsistent && !checkOptimistic)
                    return;

                if (!heuristicLoaded)
                    loadHeuristic();

                if (checkOptimistic)
                    output = checkOptimistic();

                if (checkConsistent)
                    output = checkConsistent();

            }
        }

        if (alg != null)
            output = alg.getOutput();

        System.out.println(output);
    }

    private static void handleArguments(String... args) {
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "--alg" -> algorithm = args[++i].toLowerCase();
                case "--ss" -> stateSpacePath = args[++i];
                case "--h" -> heuristicPath = args[++i];
                case "--check-optimistic" -> checkOptimistic = true;
                case "--check-consistent" -> checkConsistent = true;
                default -> System.err.println("Unexpected argument");
            }
        }

        if (algorithm == null)
            algorithm = "check";

        if (stateSpacePath == null) {
            System.err.println("No state space path given");
            System.exit(-1);
        }
    }

    private static void loadStateSpace() {
        Path path = Path.of(stateSpacePath);
        File file = path.toFile();

        if (!file.exists() || !file.canRead()) {
            System.err.println("Given state space file either doesn't exist or cannot be read");
            System.exit(-1);
        }

        try {
            List<String> allLines = Files.readAllLines(path);
            allLines = allLines.stream().filter(l -> !l.startsWith("#")).collect(Collectors.toList());

            initial = new Node(allLines.get(0).trim());

            ArrayList<Node> goalStates = new ArrayList<>();
            for (String state : allLines.get(1).split(" ")) {
                Node node = new Node(state.trim());
                goal.add(node);
                goalStates.add(node);
            }

            allLines.remove(0);
            allLines.remove(0);

            List<Future<?>> rezultati = new ArrayList<>();
            for (String transition : allLines) {
                String state = transition.split(":", 2)[0].trim();
                Node node = new Node(state);

                if (state.equals(initial.getState())) {
                    node = initial;
                } else if (goalStates.contains(node)) {
                    node = goalStates.get(goalStates.indexOf(node));
                }

                states.put(state, node);
                rezultati.add(threadPool.submit(new ParseTransitions(state, transition.split(": ", 2))));
            }

            for (Future<?> f : rezultati) {
                while (true) {
                    try {
                        f.get();
                        break;
                    } catch (InterruptedException | ExecutionException ignored) {
                    }
                }
            }
            threadPool.shutdown();

        } catch (IOException e) {
            System.err.println("IOException occurred " + e.getMessage());
            System.exit(-1);
        }
    }

    private static void loadHeuristic() {
        if (heuristicPath == null) {
            System.err.println("No heuristic path given");
            System.exit(-1);
        }
        heuristicLoaded = true;
        Path path = Path.of(heuristicPath);
        File file = path.toFile();

        if (!file.exists() || !file.canRead()) {
            System.err.println("Given heuristic file either doesn't exist or cannot be read");
            System.exit(-1);
        }

        try {
            List<String> allLines = Files.readAllLines(path);
            allLines = allLines.stream().filter(l -> !l.startsWith("#")).collect(Collectors.toList());

            for (String line : allLines) {
                String state = line.split(": ", 2)[0];
                Node node = states.get(state);
                String value = line.split(": ", 2)[1];
                node.setHeuristic(Double.parseDouble(value));
                heuristicMap.put(state, Double.parseDouble(value));
            }

        } catch (IOException e) {
            System.err.println("IOException occurred " + e.getMessage());
            System.exit(-1);
        }
    }

    private static String checkOptimistic() {
        StringBuilder sb = new StringBuilder();
        boolean isOptimistic = true;
        sb.append("# HEURISTIC-OPTIMISTIC ").append(heuristicPath)
                .append("\n");

        for (Node node : states.values()) {
            sb.append("[CONDITION]: ");
            node.setTotalCost(0.0);
            node.setParent(null);

            UCS ucs = new UCS(node);

            double h = node.getHeuristic();
            if (h <= ucs.end.getTotalCost())
                sb.append("[OK] ");
            else {
                sb.append("[ERR] ");
                isOptimistic = false;
            }

            sb.append("h(").append(node.getState()).append(") <= h*: ")
                    .append(node.getHeuristic()).append(" <= ").append(ucs.end.getTotalCost())
                    .append("\n");
        }
        sb.append("[CONCLUSION]: Heuristic is ");
        if (!isOptimistic)
            sb.append("not ");
        sb.append("optimistic.");
        return sb.toString();

    }

    private static String checkConsistent() {
        StringBuilder sb = new StringBuilder();
        boolean isConsistent = true;
        sb.append("# HEURISTIC-CONSISTENT ").append(heuristicPath)
                .append("\n");

        for (Node node : states.values()) {
            double h = node.getHeuristic();

            for (Map.Entry<String, Double> entry : transitions.get(node.getState()).entrySet()) {
                sb.append("[CONDITION]: ");
                Node other = states.get(entry.getKey());
                other.cost = entry.getValue();

                double hOther = other.getHeuristic();

                if (h <= hOther + other.getCost()) {
                    sb.append("[OK] ");
                } else {
                    sb.append("[ERR] ");
                    isConsistent = false;
                }
                sb.append("h(").append(node).append(") <= h(")
                        .append(other).append(") + c: ")
                        .append(h).append(" <= ")
                        .append(hOther).append(" + ")
                        .append(other.getCost())
                        .append("\n");
            }
        }
        sb.append("[CONCLUSION]: Heuristic is ");
        if (!isConsistent)
            sb.append("not ");
        sb.append("consistent.");
        return sb.toString();
    }

    private static class ParseTransitions implements Runnable{
        private final String node;
        private final String[] goingTo;

        public ParseTransitions(String node, String[] goingTo) {
            this.node = node;
            this.goingTo = goingTo;
        }

        @Override
        public void run() {
            Map<String, Double> toNode = new TreeMap<>();

            if (goingTo.length != 1) {
                String[] parts = goingTo[1].split(" ");
                for (String part : parts) {
                    String other = part.split(",")[0].trim();
                    Double cost = Double.parseDouble(part.split(",")[1]);
                    toNode.put(other, cost);
                }
            }
            transitions.put(node, toNode);
        }
    }
}
