package ui;

import java.util.Objects;

public class Node {
    String state;
    Node parent;
    Double cost;
    Double totalCost;
    Double heuristic;

    public Node(String state) {
        this.state = state;
        this.parent = null;
        this.cost = 0.0;
        this.totalCost = 0.0;
        this.heuristic = 0.0;
    }

    public String getState() {
        return state;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public Node getParent() {
        return parent;
    }

    public Double getCost() {
        return cost;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public Double getHeuristic() {
        return heuristic;
    }

    public void setHeuristic(Double heuristic) {
        this.heuristic = heuristic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return Objects.equals(state, node.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(state);
    }

    @Override
    public String toString() {
        return state;
    }
}

