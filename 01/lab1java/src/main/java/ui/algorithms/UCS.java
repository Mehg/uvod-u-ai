package ui.algorithms;

import ui.Node;

import java.util.*;

public class UCS extends Algorithm {
    private final TreeSet<Node> open;

    public UCS(Node start) {
        super("UCS", "", start);
        Comparator<Node> comparator = Comparator.comparing(Node::getTotalCost).thenComparing(Node::getState);
        this.open = new TreeSet<>(comparator);
        start();
    }

    @Override
    void start() {
        open.add(start);

        while (!open.isEmpty()) {
            Node node = open.pollFirst();

            visited.put(node.getState(), node);

            if (goal.contains(node)) {
                end = node;
                return;
            }

            for (Node child : successorFunction(node)) {
                if (visited.containsKey(child.getState()))
                    continue;

                double childCost = node.getTotalCost() + child.getCost();

                if (open.contains(child)) {
                    boolean removed = open.removeIf(n -> n.equals(child) && n.getTotalCost() >= childCost);
                    if (!removed)
                        continue;
                }
                child.setParent(node);
                child.setTotalCost(childCost);
                open.add(child);
            }
        }
        end = null;
    }

    @Override
    Set<Node> successorFunction(Node node) {
        Set<Node> ret = new HashSet<>();
        for (Map.Entry<String, Double> entry : transitions.get(node.getState()).entrySet()) {
            Node child = new Node(entry.getKey());
            child.setCost(entry.getValue());
            ret.removeIf(n->n.equals(child) && n.getCost()>child.getCost());
            ret.add(child);
        }
        return ret;
    }
}
