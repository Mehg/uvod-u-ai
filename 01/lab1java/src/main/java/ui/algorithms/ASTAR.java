package ui.algorithms;

import ui.Node;
import ui.Solution;

import java.util.*;

public class ASTAR extends Algorithm {
    private final TreeSet<Node> open;
    Comparator<Node> comparator = Comparator.comparing(n -> n.getTotalCost() + n.getHeuristic());


    public ASTAR(Node start, String file) {
        super("A-STAR", file, start);
        open = new TreeSet<>(comparator.thenComparing(Node::getState));
        start();
    }

    @Override
    void start() {
        open.add(start);
        while (!open.isEmpty()) {
            Node node = open.pollFirst();
            if (visited.containsKey(node.getState())) {
                continue;
            }

            visited.put(node.getState(), node);

            if (goal.contains(node)) {
                end = node;
                return;
            }

            for (Node child : successorFunction(node)) {
                if (visited.containsKey(child.getState())) {
                    Node other = visited.get(child.getState());
                    if (other.getTotalCost() < child.getTotalCost())
                        continue;
                    visited.remove(child.getState());
                }

                child.setParent(node);
                open.add(child);
            }

        }
        end = null;
    }

    @Override
    Set<Node> successorFunction(Node node) {
        Set<Node> ret = new HashSet<>();
        for (Map.Entry<String, Double> entry : transitions.get(node.getState()).entrySet()) {
            Node child = new Node(entry.getKey());
            child.setCost(entry.getValue());
            child.setHeuristic(Solution.heuristicMap.get(child.getState()));
            child.setTotalCost(node.getTotalCost() + child.getCost());
            ret.add(child);
        }
        return ret;
    }

}
