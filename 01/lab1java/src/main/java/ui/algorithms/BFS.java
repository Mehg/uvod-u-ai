package ui.algorithms;

import ui.Node;

import java.util.*;

public class BFS extends Algorithm {
    private final LinkedList<Node> open;

    public BFS(Node start) {
        super("BFS", "", start);
        this.open = new LinkedList<>();
        start();
    }

    @Override
    void start() {
        open.add(start);
        while (!open.isEmpty()) {
            Node node = open.poll();
            visited.put(node.getState(), node);

            if (goal.contains(node)) {
                end = node;
                return;
            }
            for (Node child : successorFunction(node)) {
                if (visited.containsKey(child.getState()))
                    continue;
                child.setParent(node);
                child.setTotalCost(node.getTotalCost() + child.getCost());
                open.addLast(child);
            }
        }
        end = null;
    }

    @Override
    Set<Node> successorFunction(Node node) {
        Set<Node> ret = new TreeSet<>(Comparator.comparing(Node::getState));
        for (Map.Entry<String, Double> entry : transitions.get(node.getState()).entrySet()) {
            Node child = new Node(entry.getKey());
            child.setCost(entry.getValue());
            ret.add(child);
        }
        return ret;
    }
}


