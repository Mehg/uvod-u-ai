package ui.algorithms;

import ui.Node;
import ui.Solution;

import java.util.*;

public abstract class Algorithm {
    public final Node start;
    public Node end;
    public Map<String,Node> visited;
    public Set<Node> goal;
    public Map<String, Map<String, Double>> transitions;

    private final String name;
    private final String file;

    public Algorithm(String name,String file, Node start) {
        this.name = name;
        this.start = start;
        this.file = file;
        this.visited = new HashMap<>();
        this.goal = Solution.goal;
        this.transitions = Solution.transitions;
    }

    abstract void start();

    abstract Set<Node> successorFunction(Node node);

    public String getOutput() {
        StringBuilder sb = new StringBuilder();
        sb.append("# ").append(name).append(" ").append(file).append("\n");
        sb.append("[FOUND_SOLUTION]: ");
        if (end == null) {
            sb.append("no");
            return sb.toString();
        }

        ArrayList<String> path = reconstructPath();

        sb.append("yes\n");
        sb.append("[STATES_VISITED]: ").append(visited.size()).append("\n");
        sb.append("[PATH_LENGTH]: ").append(path.size()).append("\n");
        sb.append("[TOTAL_COST]: ").append(end.getTotalCost()).append("\n");
        sb.append("[PATH]: ").append(String.join(" => ", path));
        return sb.toString();
    }

    private ArrayList<String> reconstructPath() {
        ArrayList<String> retList = new ArrayList<>();

        Node node = end;
        while (true) {
            retList.add(0, node.getState());
            Node parent = node.getParent();
            if (parent == null)
                break;
            node = parent;
        }
        return retList;
    }


}
