package ui;

import java.util.Map;

public class DataEntry {
    private final Map<String, String> attributes;
    private final String classLabel;

    public DataEntry(Map<String, String> attributes, String classLabel) {
        this.attributes = attributes;
        this.classLabel = classLabel;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public String getClassLabel() {
        return classLabel;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        for(String attribute : attributes.values())
            sb.append(attribute).append(", ");
        sb.append(classLabel).append(")");
        return sb.toString();
    }
}
