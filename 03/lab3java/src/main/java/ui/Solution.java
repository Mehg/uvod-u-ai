package ui;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Solution {
    private static Integer maxDepth;
    private static final Set<DataEntry> trainSet = new LinkedHashSet<>();
    private static final Set<DataEntry> testSet = new LinkedHashSet<>();

    private static final Map<String, Set<String>> attributesAndValues = new HashMap<>();
    private static String classLabel;

    public static void main(String... args) {
        if (args.length != 2 && args.length != 3) {
            System.err.println("Not a valid number of arguments");
            return;
        }

        readCSV(args[0], trainSet, true);
        readCSV(args[1], testSet, false);

        if (args.length == 3)
            getMaxDepth(args[2]);

        ID3 tree;
        if (maxDepth != null)
            tree = new ID3(attributesAndValues, classLabel, maxDepth);
        else tree = new ID3(attributesAndValues, classLabel);

        tree.fit(trainSet);
        tree.predict(testSet);

        System.out.println(tree.getOutput());
    }

    private static void readCSV(String file, Set<DataEntry> set, boolean train) {
        try {
            List<String> lines = Files.readAllLines(Path.of(file));
            ArrayList<String> attributeNames = new ArrayList<>(List.of(lines.get(0).split(",")));
            lines.remove(0);

            for (String line : lines) {
                Map<String, String> attributes = new HashMap<>();
                String classLabelValue = null;

                int i = 0;
                for (String part : line.split(",")) {
                    String name = attributeNames.get(i++);
                    if (i == attributeNames.size()) {
                        classLabelValue = part;
                        if (train)
                            classLabel = name;
                    } else {
                        attributes.put(name, part);
                    }

                    if (train) {
                        if (attributesAndValues.containsKey(name)) {
                            attributesAndValues.get(name).add(part);
                        } else {
                            attributesAndValues.put(name, new HashSet<>() {{
                                add(part);
                            }});
                        }
                    }
                }
                set.add(new DataEntry(attributes, classLabelValue));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void getMaxDepth(String depth) {
        try {
            maxDepth = Integer.parseInt(depth);
        } catch (NumberFormatException e) {
            System.err.println("Can't read max depth");
        }
    }


}
