package ui;

import java.util.*;
import java.util.stream.Collectors;

public class ID3 {
    private Integer maxDepth;
    private final Map<String, Set<String>> attributesAndValues;
    private final String classLabel;
    private Node root;
    private Set<DataEntry> trainSet;

    private final StringBuilder sb;


    public ID3(Map<String, Set<String>> attributesAndValues, String classLabel) {
        this.maxDepth = null;
        this.attributesAndValues = attributesAndValues;
        this.classLabel = classLabel;
        this.sb = new StringBuilder();
    }

    public ID3(Map<String, Set<String>> attributesAndValues, String classLabel, int maxDepth) {
        this(attributesAndValues, classLabel);
        this.maxDepth = maxDepth;
    }

    public void fit(Set<DataEntry> trainSet) {
        this.trainSet = new LinkedHashSet<>(trainSet);
        HashSet<String> attributeNames = new HashSet<>(attributesAndValues.keySet());
        attributeNames.remove(classLabel);
        root = id3(trainSet, new LinkedHashSet<>(trainSet), attributeNames, classLabel, 1);
        sb.append("[BRANCHES]:\n").append(printBranches(root, new ArrayList<>()));
    }

    public void predict(Set<DataEntry> testSet) {
        Map<DataEntry, String> entryPredictionMap = new HashMap<>();
        sb.append("[PREDICTIONS]: ");
        for (DataEntry entry : testSet) {
            String prediction = predict(entry);
            sb.append(prediction).append(" ");
            entryPredictionMap.put(entry, prediction);
        }
        sb.append("\n");
        getAccuracy(entryPredictionMap);
        getConfusionMatrix(entryPredictionMap);
    }

    private void getAccuracy(Map<DataEntry, String> entryStringMap) {
        int count = 0;
        for (Map.Entry<DataEntry, String> entry : entryStringMap.entrySet())
            if (entry.getKey().getClassLabel().equals(entry.getValue()))
                count++;
        sb.append("[ACCURACY]: ").append(String.format("%.5f", count * 1.0 / entryStringMap.size()));
        sb.append("\n");
    }

    private void getConfusionMatrix(Map<DataEntry, String> entryStringMap) {
        sb.append("[CONFUSION_MATRIX]:").append("\n");
        List<String> values = new ArrayList<>(attributesAndValues.get(classLabel));
        values = values.stream().sorted().collect(Collectors.toList());

        int[][] matrix = new int[values.size()][values.size()];

        for (Map.Entry<DataEntry, String> entry : entryStringMap.entrySet()) {
            int i = values.indexOf(entry.getKey().getClassLabel());
            int j = values.indexOf(entry.getValue());
            matrix[i][j] = matrix[i][j] + 1;
        }
        for (int i = 0; i < values.size(); i++) {
            for (int j = 0; j < values.size(); j++)
                sb.append(matrix[i][j]).append(" ");
            sb.append("\n");
        }
    }

    private String predict(DataEntry entry) {
        Set<DataEntry> train = new LinkedHashSet<>(trainSet);
        Node node = root;
        while (!node.isLeaf) {
            String entryValue = entry.getAttributes().get(node.value);
            if (node.transitions.containsKey(entryValue)) {
                Node finalNode = node;
                train = train.stream().filter(d -> d.getAttributes().get(finalNode.value).equals(entryValue))
                        .collect(Collectors.toSet());
                node = node.transitions.get(entryValue);
            } else
                return argmax(train);
        }
        return node.value;
    }

    public Node id3(Set<DataEntry> dataSet, Set<DataEntry> parentSet, Set<String> attributeNames, String classLabel, int depth) {
        Node retNode = new Node();

        if (dataSet.isEmpty()) {
            retNode.isLeaf = true;
            retNode.value = argmax(parentSet);
            retNode.depth = depth;
            return retNode;
        }

        String mostCommonValue = argmax(dataSet);
        Set<DataEntry> subSetWithValue = dataSet.stream().filter(d -> d.getClassLabel().equals(mostCommonValue))
                .collect(Collectors.toSet());


        if (attributeNames.isEmpty() || dataSet.equals(subSetWithValue) || (maxDepth != null && depth == maxDepth + 1)) {
            retNode.isLeaf = true;
            retNode.value = mostCommonValue;
            retNode.depth = depth;
            return retNode;
        }

        String x = "";
        double max = -2;
        for (String attributeName : attributeNames) {
            double current = getInformationGain(dataSet, attributeName, classLabel);
            if (current == max) {
                x = attributeName.compareTo(x) < 0 ? attributeName : x;
            } else if (current > max) {
                max = current;
                x = attributeName;
            }
            System.out.print("(IG " + attributeName + ")=" + current + " ");
        }
        System.out.println();

        for (String value : attributesAndValues.get(x)) {
            String finalX = x;
            Set<DataEntry> subset = dataSet.stream().filter(d -> d.getAttributes().get(finalX).equals(value))
                    .collect(Collectors.toSet());

            HashSet<String> newAttributeNames = new HashSet<>(attributeNames);
            newAttributeNames.remove(x);
            Node next = id3(subset, dataSet, newAttributeNames, classLabel, depth + 1);
            retNode.transitions.put(value, next);
        }
        retNode.value = x;
        retNode.depth = depth;

        return retNode;
    }

    private String argmax(Set<DataEntry> set) {
        Map<String, Integer> valueCountMap = getFrequencies(set, classLabel);
        OptionalInt optionalMax = valueCountMap.values().stream().mapToInt(c -> c).max();

        if (optionalMax.isEmpty())
            return attributesAndValues.get(classLabel).stream().sorted().collect(Collectors.toList()).get(0);

        int max = optionalMax.getAsInt();

        TreeSet<String> sortedSet = valueCountMap.entrySet().stream().filter(e -> e.getValue().equals(max))
                .map(Map.Entry::getKey).collect(Collectors.toCollection(TreeSet::new));
        return sortedSet.first();
    }

    private double getInformationGain(Set<DataEntry> set, String attributeName, String classLabel) {
        double gain = getEntropy(set, classLabel);

        Map<String, Integer> valueCountMap = getFrequencies(set, attributeName);

        for (Map.Entry<String, Integer> entry : valueCountMap.entrySet()) {
            Set<DataEntry> subset = set.stream().filter(e -> e.getAttributes().get(attributeName).equals(entry.getKey()))
                    .collect(Collectors.toSet());
            gain = gain - entry.getValue() * 1.0 / set.size() * getEntropy(subset, classLabel);
        }

        return gain;
    }

    private double getEntropy(Set<DataEntry> set, String attributeName) {
        int all = set.size();
        Map<String, Integer> valueCountMap = getFrequencies(set, attributeName);

        double entropy = 0;
        for (Integer value : valueCountMap.values()) {
            entropy = entropy - value * 1.0 / all * log2(value * 1.0 / all);
        }

        return entropy;
    }

    private double log2(double value) {
        return Math.log(value) / Math.log(2);
    }

    private Map<String, Integer> getFrequencies(Set<DataEntry> set, String attributeName) {
        Map<String, Integer> valueCountMap = new HashMap<>();
        for (DataEntry entry : set) {
            String value = attributeName.equals(classLabel) ? entry.getClassLabel() : entry.getAttributes().get(attributeName);
            if (valueCountMap.containsKey(value)) {
                valueCountMap.put(value, valueCountMap.get(value) + 1);
            } else
                valueCountMap.put(value, 1);
        }
        return valueCountMap;
    }

    private String printBranches(Node node, List<Node> path) {
        StringBuilder sb = new StringBuilder();
        if (node.isLeaf) {
            path.add(node);

            for (int i = 0; i < path.size() - 1; i++) {
                Node other = path.get(i);
                Node successor = path.get(i + 1);

                Optional<String> optionalKey = other.transitions.entrySet().stream()
                        .filter(e -> e.getValue().equals(successor))
                        .map(Map.Entry::getKey).findFirst();

                if (optionalKey.isEmpty())
                    throw new IllegalArgumentException("Not all arguments filled");

                String key = optionalKey.get();
                sb.append(other.depth).append(":")
                        .append(other.value).append("=")
                        .append(key).append(" ");
            }
            sb.append(node.value).append("\n");
            return sb.toString();
        }

        for (Map.Entry<String, Node> entry : node.transitions.entrySet()) {
            sb.append(printBranches(entry.getValue(), new ArrayList<>(path) {{
                add(node);
            }}));
        }
        return sb.toString();
    }

    public String getOutput() {
        return sb.toString();
    }

    private static class Node {
        private final Map<String, Node> transitions = new HashMap<>();
        private boolean isLeaf;
        private String value;
        private int depth;
    }
}
